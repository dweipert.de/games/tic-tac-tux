extends Node2D


var character: CharacterBody2D

var idx = 0
var timer = 0

var HUD = CanvasLayer.new()
var LabelTimer = preload("res://UI/HUD/Timer.tscn").instantiate()
var LevelMap = preload("res://UI/HUD/LevelMap.tscn").instantiate()
var CancelButton = preload("res://UI/HUD/CancelButton.tscn").instantiate()


func _ready():
	self.set_hud()
	get_tree().get_root().size_changed.connect(Callable(self, 'resize_hud'))
	
	character = load("res://Characters/" + Network.player.character + ".tscn").instantiate()
	self.set_player(character)
	
	Network.connect("player_changed", func(id):
		var remote_player = Network.players[id]
		
		var node_name = "%s_%s_%s" % [remote_player.name, remote_player.character, id]
		var puppet = null
		if self.has_node(node_name):
			puppet = self.get_node(node_name)
		
		if remote_player.current_level != self.idx:
			if puppet:
				puppet.queue_free()
			return
		
		if not puppet:
			puppet = load("res://Characters/" + remote_player.character + ".tscn").instantiate()
			puppet.process_mode = PROCESS_MODE_DISABLED
			self.add_child(puppet)
			puppet.name = node_name
			puppet.modulate = Color(1.0, 1.0, 1.0, 0.5)
		else:
			puppet.position = remote_player.position
			puppet.get_node("Sprite2D").play(remote_player.animation)
			puppet.get_node("Sprite2D").flip_h = remote_player.animation_flip_h
	)
	
	if has_node("ParallaxBackground"):
		$ParallaxBackground/ParallaxLayer.motion_mirroring.x = (
			$ParallaxBackground/ParallaxLayer/Sprite2D.get_rect().size
			*
			$ParallaxBackground/ParallaxLayer/Sprite2D.scale
		).x


func _process(delta):
	self.timer += delta
	self.LabelTimer.text = "%.2fs" % self.timer


func _input(event):
	if event is InputEventKey and event.keycode == KEY_ESCAPE:
		Global.open_menu()


func set_hud():
	self.HUD.add_child(self.LabelTimer)
	self.HUD.add_child(self.LevelMap)
	self.HUD.add_child(self.CancelButton)
	self.add_child(self.HUD)
	
	self.resize_hud()

func resize_hud():
	self.LabelTimer.position.x = 3
	
	self.LevelMap.position.x = get_viewport_rect().end.x - self.LevelMap.size.x
	
	self.CancelButton.position.x = get_viewport_rect().end.x - self.CancelButton.size.x
	self.CancelButton.position.y = get_viewport_rect().end.y - self.CancelButton.size.y


func set_player(character: CharacterBody2D):
	character.position.x = $Character.position.x
	character.position.y = $Character.position.y
	for child in $Character.get_children():
		if not child is Camera2D:
			$Character.remove_child(child)
	$Character.replace_by(character)


func respawn_player():
	var instance_level = Global.get_instance_level(self.idx)
	instance_level.timer = self.timer # timer continues playing
	Global.change_scene_to_instance(instance_level)


func end_level():
	Global.end_level(self)


func cancel_level():
	Global.cancel_level()

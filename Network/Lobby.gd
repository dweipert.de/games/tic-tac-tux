extends Control


var ConnectNodeContainer: Node
var ConnectNode: Node
var PlayersNodeContainer: Node
var PlayersNode: Node


func _ready():
	Network.connect("connection_succeeded", Callable(self, "_on_connection_succeeded"))
	Network.connect("connection_failed", Callable(self, "_on_connection_failed"))
	Network.connect("player_list_changed", Callable(self, "refresh_lobby"))
	Network.connect("game_error", Callable(self, "_on_game_error"))
	Network.connect("game_ended", Callable(self, "_on_game_ended"))
	$HTTPRequest.connect("request_completed", Callable(self, '_set_remote_ip'))
	
	ConnectNodeContainer = $CenterContainer
	ConnectNode = $CenterContainer/Connect
	PlayersNodeContainer = $CenterContainer2
	PlayersNode = $CenterContainer2/Players
	
	if OS.has_environment("USERNAME"):
		ConnectNode.get_node("Name").text = OS.get_environment("USERNAME")
	elif OS.has_environment("USER"):
		ConnectNode.get_node("Name").text = OS.get_environment("USER")
	else:
		var desktop_path = OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP).replace("\\", "/").split("/")
		ConnectNode.get_node("Name").text = desktop_path[desktop_path.size() - 2]
	
	# show lobby if we're already connected somewhere
	if Network.peer != null:
		self._on_connection_succeeded()
		self.set_ip()
		self.refresh_lobby()


func disable_connect_buttons(_is_disabled = true):
	pass
	#ConnectNode.get_node("Host").disabled = is_disabled
	#ConnectNode.get_node("Join").disabled = is_disabled


func refresh_lobby():
	var players = Network.players.values()
	var player = Network.player
	
	PlayersNode.get_node("List").clear()
	PlayersNode.get_node("List").add_item(player.name + " (You)")
	for p in players:
		PlayersNode.get_node("List").add_item(p.name)
	
	PlayersNode.get_node("Start").disabled = not multiplayer.is_server()


func set_ip():
	self.set_local_ip()
	self.set_remote_ip()

func set_local_ip():
	for ip in IP.get_local_addresses():
		if ip.begins_with("192.168"):
			PlayersNode.get_node("LocalIP").text = ip
			break

func set_remote_ip():
	PlayersNode.get_node("RemoteIP").text = ""
	$HTTPRequest.request("https://ipv4.icanhazip.com/")

func _set_remote_ip(_result, response_code, _headers, body):
	if response_code == 200:
		PlayersNode.get_node("RemoteIP").text = body.get_string_from_utf8()
	else:
		PlayersNode.get_node("RemoteIP").text = "Remote IP request failed!"
		self._on_game_error("Remote IP request failed!")


func _on_connection_succeeded():
	ConnectNodeContainer.hide()
	PlayersNodeContainer.show()


func _on_connection_failed():
	self.disable_connect_buttons()
	ConnectNode.get_node("Error").set_text("Connection failed!")


func _on_game_error(error):
	$ErrorDialog.dialog_text = error
	$ErrorDialog.popup_centered_clamped()
	self.disable_connect_buttons()


func _on_game_ended():
	self.show()
	ConnectNodeContainer.show()
	PlayersNodeContainer.hide()
	self.disable_connect_buttons()


func _on_Host_pressed():
	ConnectNodeContainer.hide()
	PlayersNodeContainer.show()
	ConnectNode.get_node("Error").text = ""
	
	self.set_ip()
	
	var player_name = ConnectNode.get_node("Name").text
	Network.host_game(player_name)
	self.refresh_lobby()


func _on_Join_pressed():
	var ip = ConnectNode.get_node("IP").text
	if not ip.is_valid_ip_address():
		ConnectNode.get_node("Error").text = "Invalid IP address!"
		return
	
	ConnectNode.get_node("Error").text = ""
	self.disable_connect_buttons(false)
	
	self.set_ip()
	var player_name = ConnectNode.get_node("Name").text
	Network.join_game(ip, player_name)


func _on_Start_pressed():
	Global.start_game()


func _on_Back_pressed():
	ConnectNodeContainer.show()
	PlayersNodeContainer.hide()
	Network.leave_game()

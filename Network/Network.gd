extends Node


signal connection_succeeded()
signal connection_failed()
signal player_list_changed()
signal player_changed()
signal game_error()
signal game_started()
signal game_ended()


const PORT = 10567
const MAX_PEERS = 2
var peer: ENetMultiplayerPeer = null


var player = null
var winning_player = player

# Dictionary { id = player }
var players = {}

func create_player():
	return {
		name = "",
		character = "",
		symbol = "",
		components = [],
		current_level = -1,
		position = Vector2(0, 0),
		animation = "",
		animation_flip_h = false,
	}


func _ready():
	self.player = self.create_player()
	
	multiplayer.connect("peer_connected", Callable(self, '_player_connected'))
	multiplayer.connect("peer_disconnected", Callable(self, "_player_disconnected"))
	multiplayer.connect("connected_to_server", Callable(self, "_connection_succeeded"))
	multiplayer.connect("connection_failed", Callable(self, "_connection_failed"))
	multiplayer.connect("server_disconnected", Callable(self, "_server_disconnected"))
	Global.connect("game_started", Callable(self, "_on_Global_game_started"))
	Global.connect("game_ended", Callable(self, "_on_Global_game_ended"))
	Global.connect("game_won", Callable(self, "_on_Global_game_won"))


@rpc("any_peer") func register_player(player_name):
	var id = multiplayer.get_remote_sender_id()
	
	var network_player = self.create_player()
	network_player.name = player_name
	self.players[id] = network_player
	
	emit_signal("player_list_changed")


func unregister_player(id):
	self.players.erase(id)
	emit_signal("player_list_changed")


func update_player():
	rpc("remote_update_player", self.player)
	
@rpc("any_peer") func remote_update_player(network_player):
	var id = multiplayer.get_remote_sender_id()
	self.players[id] = network_player
	emit_signal("player_changed", id)


func host_game(player_name):
	self.player.name = player_name
	self.peer = ENetMultiplayerPeer.new()
	self.peer.create_server(self.PORT, self.MAX_PEERS)
	multiplayer.multiplayer_peer = self.peer


func join_game(ip, player_name):
	self.player.name = player_name
	self.peer = ENetMultiplayerPeer.new()
	self.peer.create_client(ip, self.PORT)
	multiplayer.multiplayer_peer = self.peer


func leave_game():
	rpc("_leave_game")
	multiplayer.multiplayer_peer = null
	self.players = []


@rpc("any_peer") func _leave_game():
	var id = multiplayer.get_remote_sender_id()
	self.peer.disconnect_peer(id)


func get_player_count():
	return self.players.size() + 1


func _on_Global_game_started():
	# set symbols
	self.player.symbol = "X"
	var idx = 1
	for id in self.players:
		self.players[id].symbol = ["X", "O", "P"][idx]
		idx += 1
	
	# preconfigure game
	var all_players = self.players.duplicate()
	all_players[1] = self.player
	rpc("_preconfigure_game", {
		level_map = Global.Level_Map,
		players = all_players,
	})
	
	# start game for everyone
	rpc("_start_game")


@rpc("any_peer") func _preconfigure_game(configuration):
	Global.Level_Map = configuration.level_map
	
	self.player = configuration.players[multiplayer.get_unique_id()]
	configuration.players.erase(multiplayer.get_unique_id())
	self.players = configuration.players

@rpc("any_peer", "call_local") func _start_game():
	rpc("remote_update_player", self.player)
	emit_signal("game_started")


func _on_Global_game_ended():
	# tell all peers to end the game
	rpc("_end_game")


func _on_Global_game_won(winning_player_object):
	rpc("_end_game", winning_player_object)


@rpc("any_peer", "call_local") func _end_game(winning_player_object):
	self.winning_player = winning_player_object
	emit_signal("game_ended")


func _player_connected(_id):
	rpc("register_player", self.player.name)


func _player_disconnected(id):
	# if game is in progress:
		# self.end_game() ?
	# else
		#rpc("unregister_player", id)
	#rpc("unregister_player", id)
	self.unregister_player(id)


func _connection_succeeded():
	emit_signal("connection_succeeded")


func _connection_failed():
	multiplayer.multiplayer_peer = null # remove peer
	self.players = []
	emit_signal("connection_failed")


func _server_disconnected():
	emit_signal("game_error", "Server disconnected!")
	multiplayer.multiplayer_peer = null
	self.players = []
	#self.end_game("")

extends Control


var selected_idx = 0


func _ready():
	Global.connect("level_map_updated", Callable(self, "draw"))
	draw()


func _input(event):
	if event.is_action_pressed("DIRECTION_LEFT") and event.get_action_strength("DIRECTION_LEFT") == 1.0:
		selected_idx = max(selected_idx - 1, 0)
		set_cell_selected()
	elif event.is_action_pressed("DIRECTION_RIGHT") and event.get_action_strength("DIRECTION_RIGHT") == 1.0:
		selected_idx = min(selected_idx + 1, Global.Level_Map.size() - 1)
		set_cell_selected()
	elif event.is_action_pressed("UP") and (selected_idx - 3 >= 0) and event.get_action_strength("UP") == 1.0:
		selected_idx = max(selected_idx - 3, 0)
		set_cell_selected()
	elif event.is_action_pressed("DOWN") and (selected_idx + 3 <= Global.Level_Map.size() - 1) and event.get_action_strength("DOWN") == 1.0:
		selected_idx = min(selected_idx + 3, Global.Level_Map.size() - 1)
		set_cell_selected()
	elif event.is_action_pressed("ACCEPT"):
		Global.start_level(selected_idx)


func draw():
	var Cell = load("res://UI/LevelSelectCell.tscn")
	
	for idx in range(Global.Level_Map.size()): # size = 9
		var cell = Cell.instantiate()
		cell.level_idx = idx
		
		if idx == selected_idx:
			cell.set_selected(true)
		
		cell.set_rect_size(
			get_viewport_rect().size.x/3,
			get_viewport_rect().size.y/3
		)
		
		var column = idx % 3
		var row = idx / 3
		cell.position.x = cell.size.x*column
		cell.position.y = cell.size.y*row
		
		cell.connect("gui_input", Callable(self, '_button_pressed').bind(idx))
		
		self.add_child(cell)


func set_cell_selected():
	var cells = get_children()
	for cell in cells:
		cell.set_selected(false)
	
	cells[selected_idx].set_selected(true)


func _button_pressed(event, idx):
	if event is InputEventMouseButton or event is InputEventKey:
		if event.pressed:
			Global.start_level(idx)

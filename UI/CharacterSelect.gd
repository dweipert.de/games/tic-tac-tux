extends Control


var players_selected = {}

var bg_color_selected = '#fffba5'
var style_selected = StyleBoxFlat.new()
var style_deselected = StyleBoxFlat.new()


func _ready():
	self.style_selected.set_bg_color(self.bg_color_selected)
	self.style_deselected.set_bg_color(Color(0, 0, 0, 0))
	self.set_Wait_text()
	
	if multiplayer.is_server():
		$Panel/VBoxContainer/StartButton.visible = true


func set_Wait_text():
	$Panel/VBoxContainer/MarginContainer/Wait.text = "Waiting for " + str(Network.get_player_count() - self.players_selected.size()) + " players to choose"


func set_bg_color_selected(character, previous_character = null):
	if previous_character:
		get_node("Panel/VBoxContainer/GridContainer/" + previous_character + "Panel").set("theme_override_styles/panel", self.style_deselected)
	
	get_node("Panel/VBoxContainer/GridContainer/" + character + "Panel").set("theme_override_styles/panel", self.style_selected)


func _button_pressed(event, character):
	if event is InputEventMouseButton or event is InputEventKey:
		if event.pressed:
			rpc("_character_selected", character)
			self.set_bg_color_selected(character, Network.player.character)
			Network.player.character = character


@rpc("any_peer", "call_local") func _character_selected(character):
	var id = multiplayer.get_remote_sender_id()
	if Network.players.has(id):
		Network.players[id].character = character
	self.players_selected[id] = true
	
	self.set_Wait_text()
	if Network.get_player_count() == self.players_selected.size():
		if multiplayer.is_server():
			$Panel/VBoxContainer/StartButton.disabled = false


func _on_start_button_pressed():
	rpc("_start_game")


@rpc("any_peer", "call_local") func _start_game():
	get_tree().change_scene_to_file("res://UI/LevelSelect.tscn")

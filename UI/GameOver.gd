extends Control


func _ready():
	$Panel/VBoxContainer/Winner.text = Network.winning_player.name + " won!"


func _on_BackToLobby_pressed():
	get_tree().change_scene_to_file("res://Network/Lobby.tscn")

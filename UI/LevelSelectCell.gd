extends Panel


var level_idx = 0

var stylebox = StyleBoxFlat.new()
var background_color = "#225f30"
var background_color_selected = "#3c9f54"


func _ready():
	var level = Global.get_level(self.level_idx)
	
	stylebox = self.get("theme_override_styles/panel")
	
	$ClearMark.text = level.cleared_by.player.symbol
	$Time.text = str(level.time) + "s"
	$Name.text = level.cleared_by.player.name


func set_rect_size(x, y):
	self.size.x = x
	self.size.y = y
	
	$ClearMark.position = Vector2(0, 0)
	$ClearMark.size.x = x
	$ClearMark.size.y = y
	$ClearMark.add_theme_font_size_override('font_size', y / 2)
	
	$Time.position = Vector2(3, 3)
	
	$Name.position = Vector2(3, y - $Name.size.y)


func set_selected(is_selected = true):
	if is_selected:
		var box = stylebox.duplicate()
		box.bg_color = Color.from_string(self.background_color_selected, self.background_color)
		self.set("theme_override_styles/panel", box)
	else:
		var box = stylebox.duplicate()
		box.bg_color = Color.from_string(self.background_color, self.background_color)
		self.set("theme_override_styles/panel", box)

extends CanvasLayer
# needs to be a CanvasLayer as parent so it's drawn on top


func _ready():
	self.process_mode = PROCESS_MODE_ALWAYS


func close():
	get_tree().paused = false
	self.queue_free()


func _on_Close_pressed():
	self.close()

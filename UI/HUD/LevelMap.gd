extends Panel


func _ready():
	Global.connect("level_map_updated", Callable(self, "draw"))
	self.draw()


func draw():
	var Cell = load("res://UI/HUD/LevelMapCell.tscn")
	#var Level = get_parent().get_parent() # HUD > Level
	
	for idx in range(Global.Level_Map.size()): # size = 9
		var cell = Cell.instantiate()
		cell.level_idx = idx
		cell.set_rect_size(150/3, 150/3)
		var column = idx % 3
		var row = idx / 3
		cell.position.x = cell.size.x*column
		cell.position.y = cell.size.y*row
		
		#if idx == Level.idx:
			#cell.set_border_width_all(3)
		
		self.add_child(cell)

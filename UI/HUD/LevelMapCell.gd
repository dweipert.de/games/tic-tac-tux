extends Panel


var level_idx = 0


func _ready():
	var level = Global.get_level(self.level_idx)
	
	$ClearMark.text = level.cleared_by.player.symbol
	$Time.text = str(level.time) + "s"
	$Name.text = level.cleared_by.player.name


func set_rect_size(x, y):
	self.size.x = x
	self.size.y = y
	
	$ClearMark.position = Vector2(0, 0)
	$ClearMark.size.x = x
	$ClearMark.size.y = y
	$ClearMark.add_theme_font_size_override('font_size', y / 2)
	
	$Time.position = Vector2(3, 3)
	
	$Name.position = Vector2(3, y - ($Name.size.y / 2))

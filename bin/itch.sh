#!/bin/sh

VERSION=$(git describe --tags --abbrev=0)

godot --headless --rendering-driver opengl3 --export-release "Linux/X11" "exports/TicTacTux"
godot --headless --rendering-driver opengl3 --export-release "macOS" "exports/TicTacTux.zip"
godot --headless --rendering-driver opengl3 --export-release "Windows Desktop" "exports/TicTacTux.exe"

cd exports
zip TicTacTux-linux.zip TicTacTux TicTacTux.pck
mv TicTacTux.zip TicTacTux-osx.zip
zip TicTacTux-win.zip TicTacTux.exe TicTacTux.pck
cd ..

butler push exports/TicTacTux-linux.zip dweipert/tictactux:linux --userversion="$VERSION"
butler push exports/TicTacTux-osx.zip dweipert/tictactux:osx --userversion="$VERSION"
butler push exports/TicTacTux-win.zip dweipert/tictactux:win --userversion="$VERSION"

extends Node


const DIRECTION = {
	LEFT = -1,
	RIGHT = 1,
}

enum ACTIONS {
	DIRECTION_LEFT, DIRECTION_RIGHT,
	JUMP,
	RUN,
}
